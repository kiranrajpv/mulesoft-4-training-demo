
package com.training;


public class Flight
{

   
    private String newPrice;
   
    private String code1;
   
    private String code2;
    
    private String airlineName;
  
    private String toAirport;
   
    private String fromAirport;
    
    private String takeOffDate;
    
    private String oldPrice;
    
    private String planeType;
   
    private String seatsAvailable;

  
    public String getNewPrice() {
        return newPrice;
    }

  
    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

   
    public String getCode1() {
        return code1;
    }

   
    public void setCode1(String code1) {
        this.code1 = code1;
    }

   
    public String getCode2() {
        return code2;
    }

  
    public void setCode2(String code2) {
        this.code2 = code2;
    }

   
    public String getAirlineName() {
        return airlineName;
    }

  
    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    
    public String getToAirport() {
        return toAirport;
    }

    
    public void setToAirport(String toAirport) {
        this.toAirport = toAirport;
    }

  
    public String getFromAirport() {
        return fromAirport;
    }

  
    public void setFromAirport(String fromAirport) {
        this.fromAirport = fromAirport;
    }

  
    public String getTakeOffDate() {
        return takeOffDate;
    }

   
    public void setTakeOffDate(String takeOffDate) {
        this.takeOffDate = takeOffDate;
    }

   
    public String getOldPrice() {
        return oldPrice;
    }

   
    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

   
    public String getPlaneType() {
        return planeType;
    }

   
    public void setPlaneType(String planeType) {
        this.planeType = planeType;
    }

    
    public String getSeatsAvailable() {
        return seatsAvailable;
    }

  
    public void setSeatsAvailable(String seatsAvailable) {
        this.seatsAvailable = seatsAvailable;
    }

}
