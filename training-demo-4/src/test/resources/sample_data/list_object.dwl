[
  {
    ID: 2 as Number {class: "java.lang.Integer"},
    code1: "ER38" as String {class: "java.lang.String"},
    code2: "sd" as String {class: "java.lang.String"},
    airlineName: "american" as String {class: "java.lang.String"},
    toAirport: "LAX" as String {class: "java.lang.String"},
    fromAirport: "SFO" as String {class: "java.lang.String"},
    takeOffDate: |2003-10-01| as Date {class: "java.time.LocalDate"},
    price: 200 as Number {class: "java.lang.Integer"},
    planeType: "Boeing 737" as String {class: "java.lang.String"},
    seatsAvailable: 189 as Number {class: "java.lang.Integer"},
    totalSeats: 345 as Number {class: "java.lang.Integer"}
  } as Object {class: "java.util.LinkedHashMap"}
] 