
package com.training;

import java.util.List;

public class PriceUpdates {

    
    private String name;
   
    private List<Flight> flights = null;
    
    private List<Link> links = null;

   
    public String getName() {
        return name;
    }

   
    public void setName(String name) {
        this.name = name;
    }

   
    public List<Flight> getElements() {
        return flights;
    }

  
    public void setElements(List<Flight> flights) {
        this.flights = flights;
    }

   
    public List<Link> getLinks() {
        return links;
    }

   
    public void setLinks(List<Link> links) {
        this.links = links;
    }

}
